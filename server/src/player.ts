import { Position } from "./types";

export class Player {

    constructor(
        public readonly name: string,
        public readonly position: Position,
    ) { }

}
