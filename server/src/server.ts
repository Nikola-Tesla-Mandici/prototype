import { createSocket, RemoteInfo } from "dgram";
import { EventEmitter } from 'events';
import { KeepAliveMessage, Message, PositionMessage, UpdateMessage, WelcomeMessage } from './messages';
import { Player } from './player';
import { Potato } from './potato';
import { Client, Position, Remote } from "./types";

export interface GameServer {
    emit(event: 'new-client', client: Client<Player>): boolean
    emit(event: 'lost-client', client: Client<Player>): boolean
    emit(event: 'error', error: Error): boolean
    emit(event: 'ready'): boolean
    emit(event: 'start'): boolean
    emit(event: 'pause'): boolean
    emit(event: 'stop'): boolean

    on(event: 'new-client', listener: (client: Client<Player>) => void): this
    on(event: 'lost-client', listener: (client: Client<Player>) => void): this
    on(event: 'error', listener: (error: Error) => void): this
    on(event: 'ready', listener: () => void): this
    on(event: 'start', listener: () => void): this
    on(event: 'pause', listener: () => void): this
    on(event: 'stop', listener: () => void): this
}
export class GameServer extends EventEmitter {

    protected _potatos = new Map<string, Potato<Client<Player>>>();
    protected _update_handle: NodeJS.Timer | undefined
    protected _udp_socket = createSocket({
        type: "udp4",
        reuseAddr: true,
    })
    protected _listening = false;
    protected _paused = true

    public potato_timeout = 5000 // ms
    public position_message_frequency = 100 // ms

    constructor(
        protected readonly host: string,
        protected readonly port: number,
    ) {
        super()
        this._udp_socket.on('listening', this.handle_listening.bind(this))
        this._udp_socket.on('message', this.handle_message.bind(this))
        this._udp_socket.on('error', this.handle_error.bind(this))
        this._udp_socket.bind(port, host)
        this.on('start', () => {
            this._update_handle = setInterval(
                this.dispatch_position.bind(this),
                this.position_message_frequency)
        })
        this.on('pause', () => {
            clearInterval(this._update_handle)
            this._update_handle = undefined;
        })
    }

    public start(): void {
        this._paused = false
        this.emit('start')
    }

    public pause(): void {
        this._paused = true
        this.emit('pause')
    }

    public stop(): void {
        this.pause()
        this.emit('stop')
        this._udp_socket.close()
    }

    protected handle_listening(): void {
        this._listening = true;
        this.emit('ready')
    }

    protected handle_error(error: Error): void {
        console.error(error)
        this.emit('error', error)
    }

    protected async handle_message(raw: Buffer, remote: RemoteInfo): Promise<void> {
        if (this._paused) return
        try {
            const message: Message = JSON.parse(raw.toString())
            switch (message.type) {
                case 'welcome':
                    return await this.handle_welcome(remote, message as WelcomeMessage)
                case 'update':
                    return await this.handle_udpate(remote, message as UpdateMessage)
                case 'keep-alive':
                    return await this.handle_keep_alive(remote, message as KeepAliveMessage)
                default:
                    throw new Error(`unknown message type: ${message.type}`)
            }
        } catch (error) {
            console.error(`ERROR FROM ${remote.address}:${remote.port}`)
            return console.error(error)
        }
    }

    public dispatch_position(): void {
        const positions: { [name: string]: Position } = {};

        // fetch everyone's position
        for (const potato of this._potatos.values()) {
            const client = potato.value
            const player = client.client
            positions[Client.Id(client)] = player.position
        }

        // send to everyone
        for (const potato of this._potatos.values()) {
            const client = potato.value

            // copy the full list of positions
            const message: PositionMessage = {
                type: 'position',
                body: {
                    positions: { ...positions } // copy
                }
            }
            // remove the player from the copy of the list of positions
            delete message.body.positions[Client.Id(client)]

            this._udp_socket.send(JSON.stringify(message),
                client.port, client.address,
                (error, bytes) => {
                    if (error) throw error
                })
        }
    }

    public handle_welcome(remote: Remote, message: WelcomeMessage): void {
        const id = Client.Id(remote);
        if (!message.body.name || !message.body.position)
            throw new Error(`invalid welcome message from ${id}`)
        if (this._potatos.has(id))
            throw new Error(`player is already logged in from ${id}`)

        const player = new Player(message.body.name, message.body.position)
        const client: Client<Player> = { ...remote, client: player }
        const potato = new Potato(client, this.potato_timeout)

        potato.watch(() => {
            this._potatos.delete(id)
            this.emit('lost-client', client)
        })
        this._potatos.set(id, potato)
        this.emit('new-client', client)
    }

    public handle_udpate(remote: Remote, message: UpdateMessage): void {
        const id = Client.Id(remote)
        if (!this._potatos.has(id))
            throw new Error(`unknown player: (${id})`)

        const potato = this._potatos.get(id)
        const client = potato.value
        const player = client.client

        player.position[0] = message.body.position[0]
        player.position[1] = message.body.position[1]
        player.position[2] = message.body.position[2]

        potato.heat()
    }

    public handle_keep_alive(remote: Remote, message: KeepAliveMessage): void {
        const id = Client.Id(remote)
        if (!this._potatos.has(id))
            throw new Error(`unknown player: (${id})`)

        const potato = this._potatos.get(id)
        potato.heat()
    }

}
