from prototype.stages.test_stage import test_stage

import sys

def hook_import_debug():
    class ImportDebug:
        def find_spec(self, fullname, path, target=None):
            print('ImportDebug!', fullname, path, target)
    sys.meta_path.insert(0, ImportDebug())
# hook_import_debug()

test_stage()
