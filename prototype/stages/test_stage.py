from bgez.game import GameObject, CameraObject
from bgez.asyn import task

from bgez.game import window, scenes

from prototype.logic.player import Player
from prototype.logic.client import Client
from prototype import blends

@task
async def test_stage():
    scene = scenes.current

    await blends.Setup.load()
    player_lib = await blends.Player.load()

    player_template = player_lib.inactives['player.hitbox']
    def player_factory() -> GameObject:
        return scene.add_object(player_template)

    player: GameObject = player_factory()
    player.world_position = 0, 0, 2
    window.mouse_lock()

    camera: CameraObject = player.children['player.camera']
    camera.use().end()

    Player(player).start()
    Client(player, player_factory).start()
