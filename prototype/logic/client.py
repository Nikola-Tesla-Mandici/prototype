from typing import Dict, Callable

import asyncio
import json

from bgez.game import GameObject, LogicProcess

__all__ = [
    'Client',
]

SERVER = ('localhost', 1234)

class Client(LogicProcess):

    def __init__(self, player: GameObject, player_factory: Callable[[], GameObject]):
        player.ending.append(self.stop)

        self.player_factory = player_factory
        self.transport = None
        self.player = player

        self.players: Dict[str, GameObject] = {}

        loop: asyncio.AbstractEventLoop = asyncio.get_event_loop()
        asyncio.ensure_future(loop.create_datagram_endpoint(
            lambda: self, local_addr=('localhost', 0)))

    def connection_made(self, transport):
        self.transport = transport
        self.transport.sendto(json.dumps({
            'type': 'welcome',
            'body': {
                'name': 'paul',
                'position': list(self.player.world_position)
            }
        }).encode(), SERVER)

    def on_logic(self):
        if self.transport is None:
            return
        self.transport.sendto(json.dumps({
            'type': 'update',
            'body': {
                'position': list(self.player.world_position)
            }
        }).encode(), SERVER)

    def datagram_received(self, data, addr):
        message = json.loads(data, encoding='utf8')
        type = message['type']
        if type == 'position':
            return self.update_players(message)
        elif type == 'leave':
            return self.player_left(message)

    def update_players(self, message):
        for name, position in message['body']['positions'].items():
            if name not in self.players:
                self.players[name] = self.player_factory()
            player = self.players[name]
            player.world_position = position

    def player_left(self, message):
        print('a player left:', message)
        player = self.players.pop(message['body']['name'])
        player.end()

    def connection_lost(self, exc):
        print(exc)
