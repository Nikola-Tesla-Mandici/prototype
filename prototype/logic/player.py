from bgez.game.inputs import Keymap, AxisType, ButtonType
from bgez.game import GameObject, LogicProcess

from bge.logic import expandPath

__all__ = [
    'Player',
]

def button_to_axis_positive(button):
    return 1. if button.input.active() else 0.
def button_to_axis_negative(button):
    return -1. if button.input.active() else 0.

class Player(LogicProcess):

    controls = Keymap()

    sensitivity = .1
    speed = .1

    movementy = controls.define('movementy', AxisType).input
    movementx = controls.define('movementx', AxisType).input

    forward = controls.define('forward', ButtonType, default=['w']).input
    backward = controls.define('backward', ButtonType, default=['s']).input
    left = controls.define('left', ButtonType, default=['a']).input
    right = controls.define('right', ButtonType, default=['d']).input

    controls.link('movementy', 'forward', button_to_axis_positive)
    controls.link('movementy', 'backward', button_to_axis_negative)
    controls.link('movementx', 'right', button_to_axis_positive)
    controls.link('movementx', 'left', button_to_axis_negative)

    rotationz = controls.define('rotationz', AxisType, default=['mouse.dx']).input
    rotationx = controls.define('rotationx', AxisType, default=['mouse.dy']).input

    look_up = controls.define('look_up', ButtonType, default=['uparrow']).input
    look_down = controls.define('look_down', ButtonType, default=['downarrow']).input
    look_left = controls.define('look_left', ButtonType, default=['leftarrow']).input
    look_right = controls.define('look_right', ButtonType, default=['rightarrow']).input

    controls.link('rotationx', 'look_up', button_to_axis_positive)
    controls.link('rotationx', 'look_down', button_to_axis_negative)
    controls.link('rotationz', 'look_left', button_to_axis_positive)
    controls.link('rotationz', 'look_right', button_to_axis_negative)

    jump = controls.define('jump', ButtonType, default=['space']).input

    kill = controls.define('kill', ButtonType, default=['f']).input

    player_controls_file = expandPath('//config/player_controls.toml')
    controls.load(player_controls_file)
    controls.save(player_controls_file)

    def __init__(self, player: GameObject):
        player.ending.append(self.stop)
        self.camera = player.scene.camera
        self.player = player

    def on_logic(self):
        if self.kill.pressing():
            return self.player.end()

        if self.jump.pressing():
            self.player.apply_impulse((0, 0, 0), (0, 0, 3))

        rx = self.sensitivity * self.rotationx.value()
        rz = self.sensitivity * self.rotationz.value()
        mx = self.speed * self.movementx.threshold(.1, 0)
        my = self.speed * self.movementy.threshold(.1, 0)

        self.player.apply_rotation((0, 0, rz), local=False)
        self.camera.apply_rotation((rx, 0, 0), local=True)
        self.player.apply_movement((mx, my, 0), local=True)
