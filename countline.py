from glob import iglob

def lineCount():
    for path in iglob('./.bgez/**/*.py', recursive=True):
        with open(path) as file:
            lines = len(file.readlines())
            print(f'script {path} / lines: {lines}')
            yield lines

print(f'Total number of lines: {sum(lineCount())}')
